import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';

export class MustMatch {

    static checkMatch(control: AbstractControl) {
        const pass = control.get('password')
        const confPass = control.get('confirmPassword')

        if (pass.value != confPass.value) {
            confPass.setErrors({ 'mustMatch': true })
        }
        return null
    }
    // static checkPassword(controlName: string, matchingControlName: string) {
    //     return (formGroup: FormGroup) => {
    //         const control = formGroup.controls[controlName];
    //         const matchingControl = formGroup.controls[matchingControlName];

    //         if (matchingControl.errors && !matchingControl.errors.mustMatch) {
    //             // return if another validator has already found an error on the matchingControl
    //             return;
    //         }

    //         // set error on matchingControl if validation fails
    //         if (control.value !== matchingControl.value) {
    //             matchingControl.setErrors({ mustMatch: true });
    //         } else {
    //             matchingControl.setErrors(null);
    //         }
    //     }
    // }
}