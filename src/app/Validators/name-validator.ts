import { AbstractControl, ValidationErrors } from '@angular/forms';

export class NameValidator {
    static validateName(control: AbstractControl): ValidationErrors | null {
        if (control.value == 'test') {
            return { invalidName: true }
        }
        return null;
    }
}