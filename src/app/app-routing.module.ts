import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { UserComponent } from './user/user/user.component';
// import { UserDetailsComponent } from './user/user-details/user-details.component';
// import { AddUserComponent } from './user/add-user/add-user.component';



const routes: Routes = [
  { path: '', redirectTo: 'user', pathMatch: 'full' },
  {
    path: 'user',
    loadChildren: './user/user/user.module#UserModule'
    // loadChildren: () => import('./user/user/user.module').then(m => m.UserModule)
  },

  // {
  //   path: 'user',
  //   component: UserComponent,
  //   children: [
  //     { path: '', component: UserDetailsComponent },
  //     { path: 'add', component: AddUserComponent },
  //     // { path: 'add', component: AddUserComponent },
  //   ]
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
