import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserModule } from './user/user/user.module';
import { ReactiveFormsModule } from '@angular/forms'
// import { UserComponent } from './user/user/user.component';
// import { AddUserComponent } from './user/add-user/add-user.component';
// import { UserDetailsComponent } from './user/user-details/user-details.component';

@NgModule({
  declarations: [
    AppComponent,
    // UserComponent,
    // AddUserComponent,
    // UserDetailsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
