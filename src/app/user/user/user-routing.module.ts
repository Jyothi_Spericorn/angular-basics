import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';

import { UserDetailsComponent } from '../user-details/user-details.component';
import { AddUserComponent } from '../add-user/add-user.component';


const routes: Routes = [
    // {
    //     path: 'user',
    //     component: UserComponent
    // },

    {
        path: '',
        component: UserComponent,
        children: [
            { path: '', component: UserDetailsComponent },
            { path: 'add', component: AddUserComponent },
            // { path: 'add', component: AddUserComponent },
        ]
    },
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class UserRoutingModule { }