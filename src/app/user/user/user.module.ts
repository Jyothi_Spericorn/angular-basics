import { NgModule } from "@angular/core";
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { AddUserComponent } from '../add-user/add-user.component';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// import { AddUserComponent } from '../add-user/add-user.component';
// import { UserComponent } from './user.component';

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        ReactiveFormsModule
    ],
    declarations: [
        UserComponent,
        AddUserComponent,
        UserDetailsComponent
    ],
    exports: [
        UserComponent,
        AddUserComponent,
        UserDetailsComponent
    ]

})

export class UserModule { }