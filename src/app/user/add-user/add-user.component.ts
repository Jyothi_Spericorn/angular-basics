import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NameValidator } from 'src/app/Validators/name-validator';
import { MustMatch } from 'src/app/Validators/must-match';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  testForm: FormGroup;
  constructor() { }

  ngOnInit() {
    this.testForm = new FormGroup({
      name: new FormControl('', [Validators.required, NameValidator.validateName]),
      option: new FormControl(4),
      password: new FormControl(null, [Validators.required]),
      confirmPassword: new FormControl(null, [Validators.required])
    },
      {
        validators: MustMatch.checkMatch
      }
    )
  }

  submitForm() {
    console.log(this.testForm)
  }

  checkName(control: AbstractControl) {
    console.log(control.value)
    if (control.value != 'jk') {
      return {
        'nameNotEqual': true
      }
    }
    return null
  }

}
